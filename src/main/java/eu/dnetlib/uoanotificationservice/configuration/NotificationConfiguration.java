package eu.dnetlib.uoanotificationservice.configuration;

import eu.dnetlib.uoaauthorizationlibrary.configuration.IgnoreSecurityConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableConfigurationProperties({MongoConfig.class, GlobalVars.class})
@EnableScheduling
@ComponentScan(basePackages = { "eu.dnetlib.uoanotificationservice"})
@Import({IgnoreSecurityConfiguration.class})
public class NotificationConfiguration { }
