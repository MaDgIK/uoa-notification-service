package eu.dnetlib.uoanotificationservice.controllers;

import eu.dnetlib.uoaauthorizationlibrary.security.AuthorizationService;
import eu.dnetlib.uoanotificationservice.entities.Notification;
import eu.dnetlib.uoanotificationservice.entities.User;
import eu.dnetlib.uoanotificationservice.services.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/notification")
@CrossOrigin(origins = "*")
public class NotificationController {

    private final NotificationService notificationService;
    private final AuthorizationService authorizationService;

    @Autowired
    public NotificationController(NotificationService notificationService, AuthorizationService authorizationService) {
        this.notificationService = notificationService;
        this.authorizationService = authorizationService;
    }

    @PreAuthorize("hasAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = {"/all"}, method = RequestMethod.GET)
    public List<Notification> getAllNotifications() {
        return notificationService.getAllNotifications();
    }

    @PreAuthorize("hasAuthority(@AuthorizationService.REGISTERED_USER)")
    @RequestMapping(value = {"/{service}"}, method = RequestMethod.GET)
    public List<Notification> getMyNotifications(@PathVariable String service) {
        return notificationService.getMyNotifications(authorizationService.getAaiId(), authorizationService.getEmail(),
                service, authorizationService.getRoles());
    }

    @PreAuthorize("hasAuthority(@AuthorizationService.REGISTERED_USER)")
    @RequestMapping(value = {"/all/{service}"}, method = RequestMethod.PUT)
    public User markAllAsRead(@PathVariable String service) {
        List<Notification> notifications = notificationService.getMyNotifications(authorizationService.getAaiId(), authorizationService.getEmail(),
                service, authorizationService.getRoles());
        return notificationService.readAllNotifications(notifications, authorizationService.getAaiId());
    }

    @PreAuthorize("hasAuthority(@AuthorizationService.REGISTERED_USER) && @Utils.hasValidGroups(#notification.groups, #notification.stakeholderType)")
    @RequestMapping(value = {"/save"}, method = RequestMethod.POST)
    public Notification save(@RequestBody Notification notification) {
        notification.setUser(authorizationService.getAaiId());
        notification.setDate(new Date());
        notification.setRead(false);
        return notificationService.save(notification);
    }

    @PreAuthorize("hasAuthority(@AuthorizationService.REGISTERED_USER) && @Utils.canRead(#id)")
    @RequestMapping(value = {"/{id}"}, method = RequestMethod.PUT)
    public User readNotification(@PathVariable String id) {
        return notificationService.readNotification(authorizationService.getAaiId(), id);
    }
}

