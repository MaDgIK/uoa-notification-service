package eu.dnetlib.uoanotificationservice.controllers;

import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DBObject;
import eu.dnetlib.uoanotificationservice.configuration.GlobalVars;
import eu.dnetlib.uoanotificationservice.configuration.MongoConfig;
import eu.dnetlib.uoanotificationservice.configuration.NotificationMongoConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/notification-service")
public class NotificationServiceCheckDeployController {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final NotificationMongoConnection mongoConnection;
    private final MongoConfig mongoConfig;
    private final GlobalVars globalVars;

    @Autowired
    public NotificationServiceCheckDeployController(NotificationMongoConnection mongoConnection, MongoConfig mongoConfig, GlobalVars globalVars) {
        this.mongoConnection = mongoConnection;
        this.mongoConfig = mongoConfig;
        this.globalVars = globalVars;
    }

    @RequestMapping(value = {"", "/health_check"}, method = RequestMethod.GET)
    public String hello() {
        log.debug("Hello from uoa-notification-service!");
        return "Hello from uoa-notification-service!";
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/health_check/advanced", method = RequestMethod.GET)
    public Map<String, String> checkEverything() {
        Map<String, String> response = new HashMap<>();

        MongoTemplate mt = mongoConnection.getNotificationTemplate();
        DBObject ping = new BasicDBObject("ping", "1");
        try {
            CommandResult answer = mt.getDb().command(ping);
            response.put("Mongo try: error", answer.getErrorMessage());
        } catch (Exception e) {
            response.put("Mongo catch: error", e.getMessage());
        }

        response.put("notification.mongodb.database", mongoConfig.getDatabase());
        response.put("notification.mongodb.host", mongoConfig.getHost());
        response.put("notification.mongodb.port", String.valueOf(mongoConfig.getPort()));
        response.put("notification.mongodb.username", mongoConfig.getUsername() == null ? null : "[unexposed value]");
        response.put("notification.mongodb.password", mongoConfig.getPassword() == null ? null : "[unexposed value]");

        if(GlobalVars.date != null) {
            response.put("Date of deploy", GlobalVars.date.toString());
        }
        if(globalVars.getBuildDate() != null) {
            response.put("Date of build", globalVars.getBuildDate());
        }
        if (globalVars.getVersion() != null) {
            response.put("Version", globalVars.getVersion());
        }
        return response;
    }
}
