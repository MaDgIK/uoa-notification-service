package eu.dnetlib.uoanotificationservice.dao;

import eu.dnetlib.uoanotificationservice.entities.Notification;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface NotificationDAO extends MongoRepository<Notification, String> {

    Optional<Notification> findById(String Id);

    Optional<Notification> findByIdAndUserNotAndGroupsIn(String id, String user, Collection<String> groups);

    List<Notification> findByOrderByDateDesc();

    List<Notification> findByUserNotAndServicesInAndGroupsInOrderByDateDesc(String user, Collection<String> services, Collection<String> groups);

    List<Notification> findByDateBefore(Date date);
}
