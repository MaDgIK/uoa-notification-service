package eu.dnetlib.uoanotificationservice.dao;

import eu.dnetlib.uoanotificationservice.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserDAO extends MongoRepository<User, String> {

    List<User> findAll();

    Optional<User> findById(String Id);

    List<User> findByReadContains(String id);

    User save(User user);

    void deleteAll();

    void delete(String id);
}
