package eu.dnetlib.uoanotificationservice.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.Set;

public class User {

    @Id
    @JsonProperty("_id")
    private String id;
    private Set<String> read;

    public User() {
    }

    public User(String id, Set<String> read) {
        this.id = id;
        this.read = read;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Set<String> getRead() {
        return read;
    }

    public void setRead(Set<String> read) {
        this.read = read;
    }
}
